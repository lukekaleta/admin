import {
  Avatar,
  Button,
  Grid,
  MenuItem,
  Paper,
  Select,
  Switch,
  TextField,
  Typography,
} from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import { makeStyles } from '@material-ui/styles'
import { Form, Formik, useFormik } from 'formik'
import { useEffect } from 'react'
import { useContext, useState } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { Flag } from '../../components/Flag'
import { languages } from '../../configs'
import { AlertsContext } from '../../providers/alerts.provider'
import { AppContext } from '../../providers/app.provider'
import { AuthContext } from '../../providers/auth.provider'
import { UserContext } from '../../providers/user.provider'
import { AlertTypes } from '../../types/alerts.types'
import { LanguageTypes } from '../../types/languages.types'
import guid from '../../utils/guid'
import {
  createFormChangePasswordSchema,
  createFormPersonalInformationsSchema,
} from './Settings@config'
import { settingsPartsStyles } from './Settings@styles'
import {
  GeneralSettingsStateTypes,
  ProfileSettingsStateTypes,
} from './Settings@types'

const useStyles = makeStyles(settingsPartsStyles)

export const GeneralSettings = () => {
  // state
  const [state, setState] = useState<GeneralSettingsStateTypes>({
    openLanguageSelect: false,
    selectedLanguage: null,
  })

  const { openLanguageSelect } = state

  // context
  const { setThemeMode, getThemeMode, getLanguage, setLanguage } =
    useContext(AppContext)

  // hooks
  const classes = useStyles()
  const { t } = useTranslation()

  const theme = String(getThemeMode())
  const mode = theme === 'dark' ? true : false
  const handleSetDarkMode = () => {
    setThemeMode()
  }

  // LNAGUAGE SETTINGS
  const handleActionsLanguageSelect = () => {
    if (openLanguageSelect) {
      setState((prevState) => ({
        ...prevState,
        openLanguageSelect: false,
      }))
    } else {
      setState((prevState) => ({
        ...prevState,
        openLanguageSelect: true,
      }))
    }
  }

  return (
    <div className={classes.generalSettingsRoot}>
      <Grid container spacing={4}>
        <Grid item md={6} xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="overline" gutterBottom>
              {t`Settings.View`}
            </Typography>
            <div className={classes.spacing}>
              <Grid container spacing={1}>
                <Grid item xs={4} className={classes.middle}>
                  <Typography variant="body2">{t`Settings.Dark_Mode`}</Typography>
                </Grid>
                <Grid item xs className={classes.right}>
                  <Switch
                    checked={mode}
                    color="primary"
                    onChange={handleSetDarkMode}
                  />
                </Grid>
              </Grid>
            </div>
          </Paper>

          <Paper className={classes.paper}>
            <Typography variant="overline" gutterBottom>
              {t`Settings.Preference`}
            </Typography>
            <div className={classes.spacing}>
              <Grid container spacing={1}>
                <Grid item xs={4} className={classes.middle}>
                  <Typography variant="body2">{t`Global.Language`}</Typography>
                </Grid>
                <Grid item xs>
                  <Select
                    className={classes.fullWidth}
                    variant="outlined"
                    color="primary"
                    id="language-select"
                    labelId="language-select"
                    value={getLanguage}
                    open={openLanguageSelect}
                    onOpen={handleActionsLanguageSelect}
                    onClose={handleActionsLanguageSelect}
                    onChange={(e) => setLanguage(String(e.target.value))}
                  >
                    {languages.map((language: LanguageTypes) => (
                      <MenuItem key={language.key} value={language.value}>
                        <div className={classes.menuItem}>
                          <Flag code={language.flag} spacing={12} height={20} />
                          <Trans i18nKey={language.label} />
                        </div>
                      </MenuItem>
                    ))}
                  </Select>
                </Grid>
              </Grid>
            </div>
          </Paper>
        </Grid>
        <Grid item xs />
      </Grid>
    </div>
  )
}

export const ProfileSettings = () => {
  // state
  const [state, setState] = useState<ProfileSettingsStateTypes>({
    passwordIsIncorrect: false,
    verificationEmailSend: false,
  })
  const { passwordIsIncorrect } = state

  // context
  const { setNewPassword } = useContext(AuthContext)
  const { setAlert } = useContext(AlertsContext)
  const {
    loadUserData,
    userData,
    getFirstLetterOfUsername,
    updateProfileData,
    verifyEmailAddress,
  } = useContext(UserContext)

  // hooks
  const classes = useStyles()
  const { t } = useTranslation()

  // password form
  const formik = useFormik({
    initialValues: {
      newPassword: '',
      confirmNewPassword: '',
    },
    validationSchema: createFormChangePasswordSchema(),
    onSubmit: () => handleChangePassword(),
  })

  // personal informations form
  const personalFormik = useFormik({
    initialValues: {
      displayName: userData?.displayName || '',
      email: userData?.email || '',
      phoneNumber: userData?.phoneNumber || '',
    },
    validationSchema: createFormPersonalInformationsSchema(),
    onSubmit: () => handleChangePersonInformations(),
  })

  // effect
  useEffect(() => {
    loadUserData()
    // eslint-disable-next-line
  }, [])

  const handleChangePersonInformations = async () => {
    try {
      await updateProfileData(personalFormik.values)
      await loadUserData()
      await setAlert({
        title: t`Alert.Success`,
        description: t`Alert.Personal_Informations_Was_Changed`,
        type: AlertTypes.success,
        id: guid(),
      })
    } catch (err) {
      console.error(err)
    }
  }

  const handleChangePassword = async () => {
    if (
      formik.values.newPassword.length >= 8 &&
      formik.values.newPassword === formik.values.confirmNewPassword
    ) {
      try {
        setState((prevState) => ({
          ...prevState,
          passwordIsIncorrect: false,
        }))
        await setNewPassword({ newPassword: formik.values.newPassword })
        await formik.resetForm()
        await setAlert({
          title: t`Alert.Success`,
          description: t`Alert.Password_Was_Changed`,
          type: AlertTypes.success,
          id: guid(),
        })
      } catch (err) {
        console.error(err)
      }
    } else {
      setState((prevState) => ({
        ...prevState,
        passwordIsIncorrect: true,
      }))
    }
  }

  const handleVerifyEmailAddress = async () => {
    try {
      await verifyEmailAddress()
      await setAlert({
        title: t`Alert.Success`,
        description: t`Alert.Verify_Email_Was_Send`,
        type: AlertTypes.success,
        id: guid(),
      })
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <div className={classes.profileSettingsRoot}>
      <Grid container spacing={4}>
        <Grid item md={4} xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="overline" gutterBottom>
              {t`Settings.Profile_Image`}
            </Typography>
            <div className={classes.avatarContent}>
              <Avatar
                alt="Remy Sharp"
                src="/static/images/avatar/1.jpg"
                className={classes.avatar}
              >
                {getFirstLetterOfUsername || ''}
              </Avatar>
            </div>
            <div className={classes.textConditions}>
              <Typography variant="caption" color="textSecondary" gutterBottom>
                {t`Settings.Upload_Image_Conditions`}
              </Typography>
            </div>
            <div className={classes.uploadButton}>
              <Button
                disabled
                variant="outlined"
                size="small"
                color="secondary"
              >
                {t`Global.Upload`}
              </Button>
            </div>
          </Paper>
        </Grid>

        <Grid item md={8} xs={12}>
          <Paper className={classes.paper}>
            <Typography variant="overline" gutterBottom>
              {t`Settings.Personal_Informations`}
            </Typography>

            {!userData?.emailVerified && (
              <div className={classes.verificationAlert}>
                <Alert
                  severity="warning"
                  action={
                    <Button
                      onClick={handleVerifyEmailAddress}
                      color="inherit"
                      size="small"
                    >
                      {t`Settings.Verify_Email`}
                    </Button>
                  }
                >
                  {t`Settings.Email_Is_Not_Verified`}
                </Alert>
              </div>
            )}

            <div className={classes.personalForm}>
              <Formik
                initialValues={personalFormik.initialValues}
                onSubmit={() => personalFormik.handleSubmit()}
              >
                <Form>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        color="primary"
                        variant="outlined"
                        margin="none"
                        id="displayName"
                        name="displayName"
                        type="text"
                        label={t`Settings.Name_Surname`}
                        value={personalFormik.values.displayName}
                        onChange={personalFormik.handleChange}
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        disabled
                        fullWidth
                        required
                        color="primary"
                        variant="outlined"
                        margin="none"
                        id="email"
                        name="email"
                        type="text"
                        label={t`Settings.Email`}
                        value={personalFormik.values.email}
                        onChange={personalFormik.handleChange}
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        disabled
                        fullWidth
                        color="primary"
                        variant="outlined"
                        margin="none"
                        id="phoneNumber"
                        name="phoneNumber"
                        type="text"
                        label={t`Settings.Phone`}
                        value={personalFormik.values.phoneNumber}
                        onChange={personalFormik.handleChange}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      className={classes.personalFormSendButton}
                    >
                      <Button
                        color="primary"
                        variant="contained"
                        size="large"
                        type="submit"
                      >
                        {t`Global.Save_Changes`}
                      </Button>
                    </Grid>
                  </Grid>
                </Form>
              </Formik>
            </div>
          </Paper>

          <Paper className={classes.paper}>
            <Typography variant="overline" gutterBottom>
              {t`Settings.Change_Password`}
            </Typography>

            {passwordIsIncorrect && (
              <div className={classes.passwordAlert}>
                <Grid item xs={12}>
                  <Alert severity="error">
                    {t`Settings.Password_Is_Incorrect`}
                  </Alert>
                </Grid>
              </div>
            )}

            <div className={classes.spacing}>
              <Formik
                initialValues={formik.initialValues}
                onSubmit={() => formik.handleSubmit()}
              >
                <Form>
                  <Grid container spacing={1}>
                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        required
                        color="primary"
                        variant="outlined"
                        margin="normal"
                        id="newPassword"
                        name="newPassword"
                        type="password"
                        placeholder={t`Settings.Minimal_Password_Lenght`}
                        label={t`Settings.New_Password`}
                        value={formik.values.newPassword}
                        onChange={formik.handleChange}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        required
                        color="primary"
                        variant="outlined"
                        margin="normal"
                        id="confirmNewPassword"
                        name="confirmNewPassword"
                        type="password"
                        label={t`Settings.Confirm_New_Password`}
                        value={formik.values.confirmNewPassword}
                        onChange={formik.handleChange}
                      />
                    </Grid>
                    <Grid item xs={12} className={classes.changePasswordButton}>
                      <Button
                        color="primary"
                        variant="contained"
                        type="submit"
                        size="large"
                      >
                        {t`Global.Save_Changes`}
                      </Button>
                    </Grid>
                  </Grid>
                </Form>
              </Formik>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </div>
  )
}

export const NotificationsSettings = () => {
  // hooks
  const classes = useStyles()

  return <div className={classes.notificationsSettingsRoot}>Notifications</div>
}
