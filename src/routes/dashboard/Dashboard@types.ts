import { ReactNode } from 'react';
export type DashboardStateTypes = {
  ordersChartsYearsSelect: boolean
  ordersChartsSelectedYear: string | number | unknown
}

export type DashboardCardPropsTypes = {
  title: string
  number: string | number
  color?: string
  icon?: ReactNode
}