import { ReactNode } from 'react'

export interface ITabList {
  label: string
  value: string
}

export interface ITabPanels {
  value: string
  content: ReactNode
}
