import { createStyles, Theme } from '@material-ui/core'

export const gastroEnumsStyle = (theme: Theme) =>
  createStyles({
    root: {},
    tabPanel: {
      padding: theme.spacing(3, 0),
    },
  })