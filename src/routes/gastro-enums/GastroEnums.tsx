import { useState, ChangeEvent } from 'react'
import { useTranslation } from 'react-i18next'
import { Tab } from '@material-ui/core'
import { TabContext, TabList, TabPanel } from '@material-ui/lab'
import { makeStyles } from '@material-ui/styles'

import FoodMenuCategories from './FoodMenuCategories'
import * as config from './GastroEnums@config'
import { gastroEnumsStyle } from './GastroEnums@styles'
import { ITabList, ITabPanels } from './GastroEnums@types'

const useStyles = makeStyles(gastroEnumsStyle)

const GastroEnums = () => {
  // state
  const [value, setValue] = useState<string>(config.tabsValues.categories)

  const { t } = useTranslation()
  const classes = useStyles()

  const handleChangeTab = (event: ChangeEvent<{}>, newValue: string) => {
    setValue(newValue)
  }

  const tabsList: ITabList[] = [
    {
      label: t`Gastro_Enums.Categories`,
      value: config.tabsValues.categories
    }
  ]

  const tabPanels: ITabPanels[] = [
    {
      value: config.tabsValues.categories,
      content: <FoodMenuCategories />
    }
  ]

  return (
    <div>
      <TabContext value={value}>
        <TabList
          onChange={handleChangeTab}
          textColor="primary"
          indicatorColor="primary"
        >
          {tabsList.map((tab, index) => (
            <Tab key={index} label={tab.label} value={tab.value} />
          ))}
        </TabList>

        {tabPanels.map((tabPanel, index) => (
          <TabPanel
            color="primary"
            key={index}
            value={tabPanel.value}
            className={classes.tabPanel}
          >
            {tabPanel.content}
          </TabPanel>
        ))}
      </TabContext>
    </div>
  )
}

export default GastroEnums