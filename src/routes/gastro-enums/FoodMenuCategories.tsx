import { useContext, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Table } from '../../components/Table'
import { TableCheckIcon } from '../../components/Table/Table@parts'
import { DataContext } from '../../providers/data.provider'
import { Collections, Documents } from '../../remote/Collections'


const FoodMenuCategories = () => {
  const { t } = useTranslation()

  // context
  const { get, data, update, insert, remove, isLoading } = useContext(DataContext)
  
  // remote settins
  const collection = Collections.Food_Menu
  const documents = Documents.Categories

  const columns = [
    {
      title: t`Gastro_Enums.Cs_Name`,
      field: 'csName'
    },
    {
      title: t`Gastro_Enums.En_Name`,
      field: 'enName'
    },
    {
      title: t`Gastro_Enums.Public`,
      field: 'isPublic',
      lookup: { true: t`Global.Public`, false: t`Global.Private` },
      render: (rowData: any) => (
        <TableCheckIcon rowItem={rowData.isPublic} />
      ),
    },
  ]

  useEffect(() => {
    const refresh = () => {
      get({ collection, documents })
    }

    refresh()
    // eslint-disable-next-line
  }, [collection, documents])

  return (
    <div>
      <Table
        title={t`Gastro_Enums.Categories`}
        columns={columns}
        data={data}
        isLoading={isLoading}
        editable={{
          onRowAdd: (newData: any) =>
            new Promise((resolve, reject) => {
              const controlBoolean = newData.isPublic === 'true' ? true : false
              const data = {
                ...newData,
                isPublic: controlBoolean
              }
              insert({collection, documents, data})
              resolve(null)
            }),
          onRowDelete: async (oldData: any) =>
            new Promise((resolve, reject) => {
              remove({ collection, documents, id: oldData.id })
              resolve(null)
            }),
          onRowUpdate: (newData: any) =>
            new Promise((resolve, reject) => {
              const controlBoolean = newData.isPublic === 'true' ? true : false
              const data = {
                ...newData,
                isPublic: controlBoolean
              }
              update({ collection, documents, id: newData.id, data })
              resolve(null)
            })
        }}
      />
    </div>
  )
}

export default FoodMenuCategories