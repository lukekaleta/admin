import { Documents } from '../../remote/Collections'
import { DailyMenuDay, IDailyMenus } from '../../types/daily-menu.types'

export type DailyMenuItemsStateTypes = {
  dailyMenuDay: DailyMenuDay | null
}

export type DailyMenuDayProps = {
  documents: Documents
}

export type MenuSettingsPropsTypes = {
  documents: string
  data: IDailyMenus
  index: number
}
