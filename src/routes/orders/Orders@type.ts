import { OrdersModel } from '../../models/orders.model'

export type OrdersStateType = {
  loading: boolean
}

export type DetailModalPropsTypes = {
  rowData: OrdersModel | undefined
  handleCloseDetail: () => void
  isOpen: boolean
}
