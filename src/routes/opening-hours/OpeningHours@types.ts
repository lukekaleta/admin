export interface IOpeningHours {
  id?: string
  dayNumber: number
  day: string
  title: string
  dailyMenuOpeningFrom: Date | null
  dailyMenuOpeningTo: Date | null
  menuOpeningFrom: Date | null
  menuOpeningTo: Date | null
}

export interface IOpeningHoursList extends IOpeningHours {}

export interface IOpeningDayState extends IOpeningHours {}

export interface IOpeningDayProps extends IOpeningHoursList {
  collection: string
  documents: string
  subCollection: string
}
