import { Grid } from '@mui/material'
import { useContext, useEffect } from 'react'
import { Category } from '../../components/Category'
import { DataContext } from '../../providers/data.provider'
import { FoodMenuContext } from '../../providers/foodMenu.provider'
import { Collections, Documents } from '../../remote/Collections'

const FoodMenu = () => {
  const { get, data } = useContext(DataContext)
  const { getCategories } = useContext(FoodMenuContext)

  // remove settings
  const collection = Collections.Food_Menu
  const documents = Documents.Categories

  useEffect(() => {
    const refresh = () => {
      get({ collection, documents })
      getCategories()
    }

    refresh()
    // eslint-disable-next-line
  }, [collection, documents])

  console.log(data);

  return (
    <Grid container spacing={2}>
      {data.map((item: any) => {
        return item.isPublic && (
          <Grid key={item.id} item xs={12} md={3}>
            <Category {...item} />
          </Grid>
        )
      })}
    </Grid>
  )
}

export default FoodMenu
