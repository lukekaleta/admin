import { TextField, Switch } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { Grid, Paper, FormControlLabel } from '@mui/material'
import { useTranslation } from 'react-i18next'
import { Button } from '../../components/Button'
import { insertMenuStyles } from './FoodMenu@styles'

const useStyles = makeStyles(insertMenuStyles)

const InsertFood = () => {
  const classes = useStyles()
  const { t } = useTranslation()

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={8}>
        <Paper className={classes.paper}>
          <Grid container spacing={2}>

            <Grid item xs={12}>
              <TextField
                variant="outlined"
                label={t`Food_Menu.Name`}
                fullWidth
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                variant="outlined"
                label={t`Food_Menu.Description`}
                fullWidth
                multiline
                minRows={3}
              />
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12} md={4}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <FormControlLabel
                    control={<Switch defaultChecked color='primary' size="medium" />}
                    label={t`Food_Menu.Public`}
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    label={t`Food_Menu.Category`}
                    disabled
                    fullWidth
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    label={t`Food_Menu.Tags`}
                    fullWidth
                    multiline
                    minRows={3}
                  />
                </Grid>

                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    label={t`Food_Menu.Amount`}
                    fullWidth
                  />
                </Grid>
              </Grid>
            </Paper>
          </Grid>

          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    label={t`Food_Menu.Price`}
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" color="primary" fullWidth>{t`Global.Save`}</Button>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default InsertFood