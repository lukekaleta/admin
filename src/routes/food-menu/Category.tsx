import { useCallback, useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Grid, Typography } from '@mui/material';

import { FoodMenuContext } from '../../providers/foodMenu.provider';
import { Food } from '../../components/Food'
import { IFood } from '../../types/food-menu.types';
import { useTranslation } from 'react-i18next';
import { Button } from '../../components/Button';
import useRoute from '../../hooks/useRoute';
import { RouteName } from '../../enums/RouteName';

const Category = () => {
  const { id } = useParams<{ id: string }>();
  const { getFoodsOfCategory, foods } = useContext(FoodMenuContext)
  const { t } = useTranslation()
  const route = useRoute()

  useEffect(() => {
    const refresh = () => {
      getFoodsOfCategory(id)
    }

    refresh()
    // eslint-disable-next-line
  }, [id])

  const handleChangeRoute = useCallback(() => {
    route.push(`${RouteName.FoodMenu_InsertFood}/${id}`)
  }, [route])

  return (
    <Grid container spacing={4}>
      <Grid item xs={12}>
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={6}>
            <Typography variant="h4">{t`Food_Menu.Title`}</Typography>
          </Grid>
          <Grid item xs={6} textAlign="right">
            <Button
              variant="contained"
              color="primary"
              onClick={handleChangeRoute}
            >
              {t`Food_Menu.Add_Item`}
            </Button>
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={12}>
        <Grid container spacing={2}>
          {foods.map((food: IFood) => (
            <Food key={food.id} {...food} />
          ))}
        </Grid>
      </Grid>
    </Grid>
  )
}

export default Category