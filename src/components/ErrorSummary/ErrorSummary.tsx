import { makeStyles } from '@material-ui/styles'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import { ReactNode } from 'react'
import { ErrorSummaryStyles } from './ErrorSummary@styles'

export type ErrorSummaryStylesProps = {
  color: string
  background: string
}

export type ErrorSummaryProps = {
  errors: Record<string, string | ReactNode>
  heading?: string
} & Partial<ErrorSummaryStylesProps>

const useStyles = makeStyles(ErrorSummaryStyles)

export default function ErrorSummary(props: ErrorSummaryProps) {
  const { errors, heading = '' } = props

  const classes = useStyles()

  // prevent when there are no errors
  if (isEmpty(errors)) return null

  return (
    <div className={classes.root}>
      <div className={classes.heading}>{heading}</div>
      <ul className={classes.list}>
        {map(errors, (err, index) => (
          <li key={index}>{err}</li>
        ))}
      </ul>
    </div>
  )
}
