import { NotificationTypes } from '../../models/notifications.model'

export type NotificationsStateTypes = {
  menu: null | undefined
}

export type NotificationPropsTypes = {
  title: string
  date: number
  type: string
}

export type NotificationIconPropsTypes = {
  type: string
}
