import {
  Avatar as MuiAvatar, Card,
  Grid, List, Typography
} from '@material-ui/core'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import ListSubheader from '@material-ui/core/ListSubheader'
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn'
import CardTravelIcon from '@material-ui/icons/CardTravel'
import ContactsIcon from '@material-ui/icons/Contacts'
import DashboardIcon from '@material-ui/icons/Dashboard'
import FastfoodIcon from '@material-ui/icons/Fastfood'
import NumbersIcon from '@mui/icons-material/Numbers';
// icons
import InfoIcon from '@material-ui/icons/Info'
import LiveHelpIcon from '@material-ui/icons/LiveHelp'
import PeopleAltIcon from '@material-ui/icons/PeopleAlt'
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder'
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer'
import RestaurantMenuIcon from '@material-ui/icons/RestaurantMenu'
import { makeStyles } from '@material-ui/styles'
import { useTranslation } from 'react-i18next'
import { RouteName } from '../../enums/RouteName'
import useRoute from '../../hooks/useRoute'
import { IconDomains } from '../Icons'
import { LayoutStyles } from './Layout@styles'
import { DrawerContentPropsTypes } from './Layout@types'



const useStyles = makeStyles(LayoutStyles)

export const MainListItems = () => {
  const { t } = useTranslation()
  const route = useRoute()

  return (
    <>
      <ListItem button onClick={() => route.push(RouteName.Dashboard)}>
        <ListItemIcon>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Dashboard`} />
      </ListItem>
      <ListItem button onClick={() => route.push(RouteName.HelpCenter)}>
        <ListItemIcon>
          <LiveHelpIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Help_Center`} />
      </ListItem>
    </>
  )
}

export const ManagementMenuItems = () => {
  const { t } = useTranslation()
  const route = useRoute()

  return (
    <>
      <ListSubheader inset>{t`Menu.Management`}</ListSubheader>
      <ListItem button onClick={() => route.push(RouteName.Contacts)}>
        <ListItemIcon>
          <ContactsIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Contacts`} />
      </ListItem>
      <ListItem button onClick={() => route.push(RouteName.Tasks)}>
        <ListItemIcon>
          <AssignmentTurnedInIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Tasks`} />
      </ListItem>
      <ListItem button onClick={() => route.push(RouteName.Users)}>
        <ListItemIcon>
          <PeopleAltIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Users`} />
      </ListItem>
    </>
  )
}

export const WebsitesMenuItems = () => {
  const { t } = useTranslation()
  const route = useRoute()

  return (
    <>
      <ListSubheader inset>{t`Menu.Web_Management`}</ListSubheader>
      <ListItem button onClick={() => route.push(RouteName.Orders)}>
        <ListItemIcon>
          <CardTravelIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Orders`} />
      </ListItem>
      <ListItem button onClick={() => route.push(RouteName.Domains)}>
        <ListItemIcon>
          <IconDomains size="24" />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Domains`} />
      </ListItem>
      <ListItem button onClick={() => route.push(RouteName.Question)}>
        <ListItemIcon>
          <QuestionAnswerIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Questions`} />
      </ListItem>
    </>
  )
}

export const GastronomyMenuItems = () => {
  const { t } = useTranslation()
  const route = useRoute()

  return (
    <>
      <ListSubheader inset>{t`Menu.Gastronomy`}</ListSubheader>
      <ListItem button onClick={() => route.push(RouteName.FoodMenu)}>
        <ListItemIcon>
          <RestaurantMenuIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Food_Menu`} />
      </ListItem>
      <ListItem button onClick={() => route.push(RouteName.DailyMenu)}>
        <ListItemIcon>
          <FastfoodIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Daily_Menu`} />
      </ListItem>
      <ListItem button onClick={() => route.push(RouteName.OpeningHours)}>
        <ListItemIcon>
          <QueryBuilderIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Opening_Hours`} />
      </ListItem>
      <ListItem button onClick={() => route.push(RouteName.GastroEnums)}>
        <ListItemIcon>
          <NumbersIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.Enums`} />
      </ListItem>
    </>
  )
}

export const AppMenuItems = () => {
  const { t } = useTranslation()
  const route = useRoute()

  return (
    <>
      <ListSubheader inset>{t`Menu.Application`}</ListSubheader>
      <ListItem button onClick={() => route.push(RouteName.About)}>
        <ListItemIcon>
          <InfoIcon />
        </ListItemIcon>
        <ListItemText primary={t`Menu.About_App`} />
      </ListItem>
    </>
  )
}

export const DrawerContent = (props: DrawerContentPropsTypes) => {
  // props
  const {
    getSiderNavWidth,
    logoUrl,
    getFirstLetterOfUsername,
    userData,
  } = props
  // hooks
  const { t } = useTranslation()
  const classes = useStyles({ getSiderNavWidth })

  return (
    <>
      <div className={classes.logo}>
        <img src={`/images/admin-logo-${logoUrl}.png`} alt="logo" />
      </div>
      <div className={classes.profile}>
        <Card className={classes.profileCard} variant="outlined">
          <Grid container spacing={2}>
            <Grid item xs={3} className={classes.profileCardAvatar}>
              <MuiAvatar>{getFirstLetterOfUsername}</MuiAvatar>
            </Grid>
            <Grid item xs={3}>
              <Typography variant="subtitle1" display="block">
                {userData?.displayName || t`Global.Not_Filled`}
              </Typography>
              <Typography variant="caption" display="block">
                Grand Admin
              </Typography>
            </Grid>
          </Grid>
        </Card>
      </div>
      <List>
        <MainListItems />
      </List>
      <List>
        <ManagementMenuItems />
      </List>
      <List>
        <WebsitesMenuItems />
      </List>
      <List>
        <GastronomyMenuItems />
      </List>
      <List>
        <AppMenuItems />
      </List>
    </>
  )
}
