export type FlagPropsTypes = {
  code: string
  format?: 'png' | 'svg'
  width?: number
  height?: number
  spacing?: number
}