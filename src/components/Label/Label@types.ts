export type LabelPropsTypes = {
  label: string
  value?: string | number
  checker?: boolean
}