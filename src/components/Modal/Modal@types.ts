import { ReactNode } from 'react'

export type ModalPropsTypes = {
  open: boolean
  children: ReactNode
  handleClose: () => void
}
