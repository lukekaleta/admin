import { CSSProperties } from '@material-ui/styles'
import { ButtonProps as MuiButtonProps } from '@material-ui/core/Button'
import { ReactText } from 'react'

export type ButtonStylesProps = {
  direction: 'row' | 'column'
  size: 'small' | 'medium' | 'large'
  justifyContent: CSSProperties['justifyContent']
}

export type ButtonProps = Omit<MuiButtonProps, 'color' | 'size' | 'variant'> & {
  color?: 'primary' | 'secondary' | 'inherit' | 'default'
  variant?: MuiButtonProps['variant'] | 'custom'
  textTransform?: 'none' | 'lowercase' | 'uppercase'
  // background?: Coloring
  backgroundOpacity?: number
  paddingX?: number
  paddingY?: number
  marginX?: ReactText | false
  marginY?: ReactText | false
  marginTop?: ReactText
  marginBottom?: ReactText
  textAlign?: 'left' | 'center' | 'right'
  noStyles?: boolean
  maxWidth?: ReactText
  minWidth?: ReactText
  borderSize?: number
} & Partial<ButtonStylesProps>
