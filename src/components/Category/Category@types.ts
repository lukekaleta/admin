export interface ICategoryProps {
  id: string
  csName: string
  enName: string
  isPublic: string
  queryKey: string
}