import { createStyles, Theme } from '@material-ui/core'

export const CategoryStyles = (theme: Theme) =>
  createStyles({
    root: {},
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column',
      height: 'auto',
      border: '1px solid',
      borderColor: 'transparent',

      '&:hover': {
        boxShadow: theme.shadows[6]
      }
    },
    pointer: {
      cursor: 'pointer',
    },
  })
