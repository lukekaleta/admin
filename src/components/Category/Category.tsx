import { makeStyles, Typography } from '@material-ui/core'
import { Paper } from '@mui/material'
import classnames from 'classnames'
import { useCallback } from 'react'
import { RouteName } from '../../enums/RouteName'
import useRoute from '../../hooks/useRoute'

import { CategoryStyles } from './Category@styles'
import { ICategoryProps } from './Category@types'

const useStyles = makeStyles(CategoryStyles)

const Category = (props: ICategoryProps) => {
  const classes = useStyles()
  const route = useRoute()

  const handleClick = useCallback(() => {
    route.push(`${RouteName.FoodMenu_Category}/${props.id}`)
  }, [route, props.id])

  return (
    <Paper onClick={handleClick} className={classnames([classes.paper, classes.pointer])}>
      <Typography
        variant="h6"
        component="div"
        align="center"
      >
        {props.csName}
      </Typography>
    </Paper>
  )
}

export default Category