import { Card, CardContent, CardHeader, CardMedia, Grid, IconButton, ListItemIcon, MenuList, Typography } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import DeleteIcon from '@mui/icons-material/Delete';
import ListAltIcon from '@mui/icons-material/ListAlt';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { useState } from 'react';
import { IFoodProps, IFoodState } from './Food@types';
import { formatLongerTimestamp } from '../../utils/datetime';
import { useTranslation } from 'react-i18next';

const Food = (props: IFoodProps) => {
  const [state, setState] = useState<IFoodState>({
    menu: null
  })

  const { t } = useTranslation()

  const open = Boolean(state.menu);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setState((prevState) => ({
      ...prevState,
      menu: event.currentTarget
    }))
  };
  const handleClose = () => {
    setState((prevState) => ({
      ...prevState,
      menu: null
    }))
  };

  const MenuComponent = () => {
    return (
      <Menu
        id="menu"
        MenuListProps={{
          'aria-labelledby': 'menu-button',
        }}
        anchorEl={state.menu}
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            width: '20ch',
          },
        }}
      >
        <MenuList>
          <MenuItem onClick={handleClose} disableRipple>
            <ListItemIcon>
              <ListAltIcon fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">
              {t`Global.Detail`}
            </Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} disableRipple>
            <ListItemIcon>
              <EditIcon fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">
              {t`Global.Edit`}
            </Typography>
          </MenuItem>
          <MenuItem onClick={handleClose} disableRipple>
            <ListItemIcon>
              <DeleteIcon color="error" fontSize="small" />
            </ListItemIcon>
            <Typography color="error" variant="inherit">
              {t`Global.Remove`}
            </Typography>
          </MenuItem>
        </MenuList>
      </Menu>
    )
  }

  return (
    <Grid item xs={3}>
      <Card sx={{ maxWidth: 345 }}>
        <CardHeader
          title={props.name}
          subheader={formatLongerTimestamp(props.createdAt)}
          action={
            <>
              <IconButton
                id="menu-button"
                onClick={handleClick}
                aria-label="settings"
              >
                <MoreVertIcon />
              </IconButton>

              <MenuComponent />
            </>
          }
        />
        <CardMedia
          component="img"
          height="194"
          image="https://picsum.photos/seed/picsum/296/194"
          alt="Paella dish"
        />
        <CardContent>
          <Typography sx={{ mb: 1.5 }}>
            {t`Food_Menu.Description`}
          </Typography>
          <Typography variant="body2">
            {props.description}
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  )
}

export default Food