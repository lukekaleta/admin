import { Theme, createStyles } from '@material-ui/core'

export const foodStyles = (theme: Theme) =>
  createStyles({
    root: {
      width: '100%'
    },
    paper: {
      padding: theme.spacing(2),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column',
      height: 'auto',
      border: '1px solid',
      borderColor: 'transparent',
    },
  })
