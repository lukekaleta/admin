import { IFood } from '../../types/food-menu.types'

export interface IFoodProps extends IFood {}
export interface IFoodState {
  menu: null | HTMLElement
}