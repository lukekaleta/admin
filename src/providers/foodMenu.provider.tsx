import { createContext, ReactNode, useState } from 'react';

import firebase from '../remote/Firebase'
import { Collections, Documents, SubCollections } from '../remote/Collections';
import { IFoodMenuCategory, IFood } from '../types/food-menu.types';

interface IFoodMenuProviderState {
  isLoading: boolean
  categories: IFoodMenuCategory[] | []
  foods: IFood[] | []
}

interface IContext extends IFoodMenuProviderState {
  getCategories: () => void
  getFoodsOfCategory: (id: string) => void
}

interface IFoodMenuProviderProps {
  children: ReactNode
}

export const FoodMenuContext = createContext<IContext>(null as any)

const FoodMenuProvider = (props: IFoodMenuProviderProps) => {
  // state
  const [state, setState] = useState<IFoodMenuProviderState>({
    isLoading: true,
    categories: [],
    foods: []
  })
  const { isLoading, categories, foods } = state

  // props
  const { children } = props

  // remote settings
  const db = firebase.firestore()
  const collection = Collections.Food_Menu
  const documents = Documents.Categories
  const subCollection = SubCollections.Data

  const cleaningState = () => {
    setState({
      isLoading: true,
      categories: [],
      foods: []
    })
  }

  const getCategories = () => {
    cleaningState()

    return db
      .collection(collection)
      .doc(documents)
      .collection(subCollection)
      .onSnapshot((s) => {
        const listItems = s.docs.map((doc: any) => ({
          id: doc.id,
          ...doc.data()
        }))
        setState((prevState) => ({
          ...prevState,
          categories: listItems,
          isLoading: false
        }))
      })
  }

  const getFoodsOfCategory = (id: string) => {
    cleaningState()

    return db
      .collection(collection)
      .doc(documents)
      .collection(subCollection)
      .doc(id)
      .collection(subCollection)
      .onSnapshot((s) => {
        const listItems = s.docs.map((doc: any) => ({
          id: doc.id,
          ...doc.data()
        }))
        setState((prevState) => ({
          ...prevState,
          foods: listItems,
          isLoading: false
        }))
      })
  }

  return (
    <FoodMenuContext.Provider
      value={{
        isLoading,
        categories,
        foods,
        getCategories,
        getFoodsOfCategory
      }}
    >
      {children}
    </FoodMenuContext.Provider>
  )
}

export default FoodMenuProvider