export interface IFoodMenuCategory {
  id?: string
  csName: string
  enName: string
  queryKey: string
  isPublic: boolean
}

export interface IFood {
  id?: string
  name: string
  amount: number
  price: number
  description: string
  amountType: string
  isPublic: boolean
  createdAt: number
  tags: string[]
}
