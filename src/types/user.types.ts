export interface UserData {
  displayName: string
  email: string
  phoneNumber: string
}

export interface User {
  id?: string
  fullName?: string
  profession?: string
  createdAt?: string
  lastLoginAt?: string
  verified?: boolean
  initials?: string
  photoUrl?: string | null
  coverUrl?: string
  roles?: string[] | []
}
