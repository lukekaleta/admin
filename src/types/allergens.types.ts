interface IAllergens {
  number: number
  identifier: string
  loc: string
}

export default IAllergens
