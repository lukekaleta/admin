export type TableColumnsTypes = {
  title: string
  field: string
}
