//  DATA PROVIDER
export interface FirebaseRootTypes {
  collection: string
  documents: string
  subCollection?: string
  orderBy?: string
}

export interface GetRecordTypes extends FirebaseRootTypes {
  id: string
}

export interface InsertTypes extends FirebaseRootTypes {
  data: object
}

export interface UpdateTypes extends FirebaseRootTypes {
  data: object
  id: string
}

export interface RemoveTypes extends FirebaseRootTypes {
  id: string
}

// USER PROVIDER
export interface UserDataTypes {
  fullName: string
  email: string
  emailVerified: boolean
  avatarUrl: string
  phoneNumber: number
  refreshToken: string
  uid: string
}
