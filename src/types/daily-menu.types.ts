// @deprecated
export interface MenuData {
  id?: string
  menuName: string
  menuPrice: string
  menuAmount: string
  menuAllergens: string[] | []
  hasMinute: boolean
  hasBusiness: boolean
}

export interface DailyMenuDay {
  menuDate: number
  lastUpdate: number
  hasDayCanceled?: boolean
  reasonForCancel?: string
  soapName: string
  soapPrice: string
  soapAllergens: string
  soapAmount: number
}

export interface DailyMenuData {
  monday: DailyMenuDay
  tuesday: DailyMenuDay
  wednesday: DailyMenuDay
  thursday: DailyMenuDay
  friday: DailyMenuDay
  saturday: DailyMenuDay
  sunday: DailyMenuDay
}
/**************** NEW ****************/
export interface IDailyMenus {
  id?: string
  menuName: string
  menuPrice: string
  menuAmount: number
  menuAllergens: string[] | []
  isMenuActive: boolean
  isMinute: boolean
  isBusiness: boolean
}

export interface IDailyMenuSoups {
  id?: string
  isSoupActive: boolean
  soupName: string
  soupPrice: string
  soupAllergens: string[] | []
  soupAmount: number
}

export interface IDailyMenuInfoNew {
  menuDate: number
  lastUpdate: number
  reasonForCancel: string
  extraText: string
  isCanceled: boolean
}
