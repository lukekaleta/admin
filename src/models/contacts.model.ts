export type ContactsModel = {
  id?: string
  fullname: string
  phone: string
  email: string
  website: string
  note: string
  activity: string
  createdAt: number
}