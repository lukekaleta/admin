export type QuestionsModel = {
  name: string
  surname: string
  email: string
  phone: string
  createdAt: number
  message: string
  hasSolved: boolean
}